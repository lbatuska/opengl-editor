# Opengl Editor

The goal is to make a small "editor" which is able to instantiate 3D primitives, load models, change their position, scale and texture in runtime.

## Using

opengl
glad
glfw
glm
imgui
