#version 410 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTex;


// out color to Frag shader
out vec3 color;
// out Texture for Frag shader
out vec2 texCoord;

uniform mat4 camMatrix;


void main()
{
	gl_Position = camMatrix * vec4(aPos, 1.0);
	// Send color straight to Frag shader
	color = aColor;
	// Send textures straight to Frag shader
	texCoord = aTex;
}