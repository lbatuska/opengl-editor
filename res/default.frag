#version 410 core

// Frag output
out vec4 FragColor;

// Input from Vertex shader
in vec3 color;
// Input from Vertex shader
in vec2 texCoord;

// Texture from main
uniform sampler2D tex0;


void main()
{
	FragColor = texture(tex0, texCoord);
}