#include "VAO.h"

#include <iostream>

VAO::VAO()
{
	glGenVertexArrays(1, &ID);
	std::cout << "VAO " << ID << " constructed!\n";
}
VAO::~VAO()
{
	std::cout << "VAO " << ID << " destructed!\n";
}
auto VAO::GetID() const -> GLuint
{
	return ID;
}
auto VAO::Bind() const -> void
{
	glBindVertexArray(ID);
}
auto VAO::Unbind() const -> void
{
	glBindVertexArray(0);
}
auto VAO::Delete() const -> void
{
	glDeleteVertexArrays(1, &ID);
	std::cout << "VAO " << ID << " buffer deleted!\n";
}

auto VAO::LinkAttrib(VBO &VBO, GLuint layout, GLuint numComponents, GLenum type, GLsizeiptr stride, void *offset) -> void
{
	VBO.Bind();
	glVertexAttribPointer(layout, numComponents, type, GL_FALSE, stride, offset);
	glEnableVertexAttribArray(layout);
	VBO.Unbind();
}