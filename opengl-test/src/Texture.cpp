#include "Texture.h"

auto Texture::GetID() const -> GLuint
{
	return ID;
}

auto Texture::Bind() const -> void
{
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(type, ID);
}

auto Texture::Unbind() const -> void
{
	glBindTexture(type, 0);
}

auto Texture::Delete() const -> void
{
	glDeleteTextures(1, &ID);
	std::cout << "Texture " << ID << " buffer deleted!\n";
}

Texture::Texture(const char *image, GLenum texType, GLuint slot, GLenum format, GLenum pixelType) : type(texType)
{
	// type = texType;
	std::cout << "Texture path: " << image << "\n";
	int widthImg;
	int heightImg;
	int numColCh;
	// Flips the image so it appears right side up
	stbi_set_flip_vertically_on_load((int)true);
	unsigned char *bytes = stbi_load(image, &widthImg, &heightImg, &numColCh, 0);
	glGenTextures(1, &ID);
	// Assigns the texture to a Texture Unit
	glActiveTexture(GL_TEXTURE0 + slot);
	unit = slot;
	glBindTexture(texType, ID);
	// Scaling
	glTexParameteri(texType, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(texType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// Repeat if any
	glTexParameteri(texType, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(texType, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Assigns the image to the OpenGL Texture object
	glTexImage2D(texType, 0, GL_RGBA, widthImg, heightImg, 0, format, pixelType, bytes);
	// Generates MipMaps
	glGenerateMipmap(texType);

	stbi_image_free(bytes);
	glBindTexture(texType, 0);
	std::cout << "Texture " << ID << " constructed"
			  << "\n";
}

Texture::~Texture()
{
	std::cout << "Texture " << ID << " destructed!\n";
}

auto Texture::texUnit(Shader &shader, const char *uniform, GLuint unit) -> void
{
	// uniform location
	GLuint texUni = glGetUniformLocation(shader.GetID(), uniform);
	// Activate shader before changing uniform
	shader.Activate();
	// Set new uniform value
	glUniform1i(texUni, unit);
}

auto Texture::GetType() const -> GLenum
{
	return type;
}