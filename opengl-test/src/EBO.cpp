#include "EBO.h"

#include <iostream>

auto EBO::GetID() const -> GLuint
{
	return ID;
}
auto EBO::Bind() const -> void
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
}
auto EBO::Unbind() const -> void
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

auto EBO::Delete() const -> void
{
	glDeleteBuffers(1, &ID);
	std::cout << "EBO " << ID << " buffer deleted!\n";
}

EBO::EBO(std::vector<GLuint> &indices)
{
	glGenBuffers(1, &ID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);
	std::cout << "EBO " << ID << " constructed!\n";
}

EBO::EBO(GLuint *indices, GLsizeiptr size)
{
	glGenBuffers(1, &ID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, GL_STATIC_DRAW);
	std::cout << "EBO " << ID << " constructed!\n";
}

EBO::~EBO()
{
	std::cout << "EBO " << ID << " destructed!\n";
}
