#include "Camera.h"

Camera::Camera(int width, int height, glm::vec3 position)
	: width(width), height(height), Position(position),
	  Orientation(glm::vec3(0.0F, 0.0F, -1.0F)),
	  Up(glm::vec3(0.0F, 1.0F, 0.0F)), cameraMatrix(glm::mat4(1.0F)),
	  firstClick(true), speed(0.1F), sensitivity(100.0F),
	  FOV(45.5F), nearPlane(0.1F), farPlane(100.0F)
{
}
// constructor delegation
Camera::Camera(const Engine &engine, glm::vec3 position)
	: Camera(engine.GetWindowSize().first, engine.GetWindowSize().second, position)
{
}

auto Camera::UpdateMatrix(float FOVdeg, float nearPlane, float farPlane) -> void
{
	// Initializes matrices since otherwise they will be the null matrix
	glm::mat4 view = glm::mat4(1.0F);
	glm::mat4 projection = glm::mat4(1.0F);

	// Makes camera look in the right direction from the right position
	view = glm::lookAt(Position, Position + Orientation, Up);
	// Adds perspective to the scene
	projection = glm::perspective(glm::radians(FOVdeg), (float)width / height, nearPlane, farPlane);

	cameraMatrix = projection * view;
}
auto Camera::UpdateMatrix() -> void
{
	// Initializes matrices since otherwise they will be the null matrix
	glm::mat4 view = glm::mat4(1.0F);
	glm::mat4 projection = glm::mat4(1.0F);

	// Makes camera look in the right direction from the right position
	view = glm::lookAt(Position, Position + Orientation, Up);
	// Adds perspective to the scene
	projection = glm::perspective(glm::radians(FOV), (float)width / height, nearPlane, farPlane);

	cameraMatrix = projection * view;
}

// returns the view matrix calculated using Euler Angles and the LookAt Matrix
auto Camera::GetViewMatrix() -> glm::mat4
{
	return glm::lookAt(Position, Position + Orientation, Up);
}

auto Camera::GetProjectionMatrix() -> glm::mat4
{
	return glm::perspective(glm::radians(FOV), (float)width / height, nearPlane, farPlane);
}

auto Camera::Matrix(Shader &shader, const char *uniform) -> void
{
	// Exports the camera matrix to the Vertex Shader
	glUniformMatrix4fv(glGetUniformLocation(shader.GetID(), uniform), 1, GL_FALSE, glm::value_ptr(cameraMatrix));
}

void Camera::Matrix(float FOVdeg, float nearPlane, float farPlane, Shader &shader, const char *uniform)
{
	// Initializes matrices since otherwise they will be the null matrix
	glm::mat4 view = glm::mat4(1.0F);
	glm::mat4 projection = glm::mat4(1.0F);

	// Makes camera look in the right direction from the right position
	view = glm::lookAt(Position, Position + Orientation, Up);
	// Adds perspective to the scene
	projection = glm::perspective(glm::radians(FOVdeg), (float)width / height, nearPlane, farPlane);

	// Exports the camera matrix to the Vertex Shader
	glUniformMatrix4fv(glGetUniformLocation(shader.GetID(), uniform), 1, GL_FALSE, glm::value_ptr(projection * view));
}

void Camera::Inputs(GLFWwindow *window)
{
	// Handles key inputs
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		Position += speed * Orientation;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		Position += speed * -glm::normalize(glm::cross(Orientation, Up));
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		Position += speed * -Orientation;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		Position += speed * glm::normalize(glm::cross(Orientation, Up));
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		Position += speed * Up;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		Position += speed * -Up;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		speed = 0.4F;
	}
	else if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	{
		speed = 0.1F;
	}

	// Handles mouse inputs
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
	{
		// Hides mouse cursor
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

		// Prevents camera from jumping on the first click
		if (firstClick)
		{
			glfwSetCursorPos(window, (width / 2), (height / 2));
			firstClick = false;
		}

		// Stores the coordinates of the cursor
		double mouseX;
		double mouseY;
		// Fetches the coordinates of the cursor
		glfwGetCursorPos(window, &mouseX, &mouseY);
		// Normalizes and shifts the coordinates of the cursor such that they begin in the middle of the screen
		// and then "transforms" them into degrees
		float rotX = sensitivity * (float)(mouseY - (height / 2)) / height;
		float rotY = sensitivity * (float)(mouseX - (width / 2)) / width;

		// Calculates upcoming vertical change in the Orientation
		glm::vec3 newOrientation = glm::rotate(Orientation, glm::radians(-rotX), glm::normalize(glm::cross(Orientation, Up)));

		// Decides whether or not the next vertical Orientation is legal or not
		if (std::abs(glm::angle(newOrientation, Up) - glm::radians(90.0F)) <= glm::radians(85.0F))
		{
			Orientation = newOrientation;
		}

		// Rotates the Orientation left and right
		Orientation = glm::rotate(Orientation, glm::radians(-rotY), Up);

		// Sets mouse cursor to the middle of the screen so that it doesn't end up roaming around
		glfwSetCursorPos(window, (width / 2), (height / 2));
	}
	else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE)
	{
		// Unhides cursor since camera is not looking around anymore
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		// Makes sure the next time the camera looks around it doesn't jump
		firstClick = true;
	}
}

auto Camera::DrawUIElement() const -> void
{
	bool open = true;
	ImGui::Begin("Camera", &open);
	ImGui::Text("FOV");
	ImGui::SameLine();
	ImGui::Text(std::to_string(FOV).data());
	ImGui::Text("nearPlane");
	ImGui::SameLine();
	ImGui::Text(std::to_string(nearPlane).data());
	ImGui::Text("farPlane");
	ImGui::SameLine();
	ImGui::Text(std::to_string(farPlane).data());
	ImGui::Text("Position");
	ImGui::SameLine();
	ImGui::Text(glm::to_string(Position).data());
	ImGui::Text("Orientattion");
	ImGui::SameLine();
	ImGui::Text(glm::to_string(Orientation).data());
	ImGui::Text("CameraMatrix");
	ImGui::SameLine();
	// Pretty long because it's mat4, but we won't waste cpu time on splitting it
	ImGui::Text(glm::to_string(cameraMatrix).data());
	ImGui::End();
}