#include "VBO.h"

#include <iostream>

VBO::VBO(std::vector<Vertex> &vertices)
{
	glGenBuffers(1, &ID);
	glBindBuffer(GL_ARRAY_BUFFER, ID);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
	std::cout << "VBO " << ID << " constructed!\n";
}

VBO::VBO(GLfloat *vertices, GLsizeiptr size)
{
	glGenBuffers(1, &ID);
	glBindBuffer(GL_ARRAY_BUFFER, ID);
	glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
	std::cout << "VBO " << ID << " constructed!\n";
}

VBO::~VBO()
{
	std::cout << "VBO " << ID << " destructed!\n";
}
auto VBO::GetID() const -> GLuint
{
	return ID;
}
auto VBO::Bind() const -> void
{
	glBindBuffer(GL_ARRAY_BUFFER, ID);
}
auto VBO::Unbind() const -> void
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
auto VBO::Delete() const -> void
{
	glDeleteBuffers(1, &ID);
	std::cout << "VBO " << ID << " buffer deleted!\n";
}