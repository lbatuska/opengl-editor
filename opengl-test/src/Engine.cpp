#include "Engine.h"

auto Engine::glfw_error_callback(int error, const char *description) -> void
{
    std::cout << "Glfw Error " << error << ": " << description << "\n";
};

Engine::Engine(unsigned int width, unsigned int height,
               const char *title, ImVec4 clear_color)
    : width(width), height(height),
      deltaTime(0.0F), lastFrameTime(0.0F),
      framesSinceUpdate(0), accumulateDelta(0.0F),
      shouldExit(false), title(title),
      glsl_version("#version 410"),
      clear_color(clear_color)
{
    std::cout << "Engine constructed\n";
}
Engine::~Engine()
{
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    // Delete window before ending the program
    // glfwDestroyWindow(window.get());
    std::cout << "Engine destructed\n";
}

auto Engine::Initialize() -> void
{
    // Initialize GLFW
    glfwInit();
    glfwSetErrorCallback(glfw_error_callback);

    // Tell GLFW what version of OpenGL we are using
    // In this case we are using OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    // Only use this if you don't have a framebuffer
    // glfwWindowHint(GLFW_SAMPLES, samples);
    // Tell GLFW we are using the CORE profile
    // So that means we only have the modern functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create a GLFWwindow object of 800 by 800 pixels, naming it "YoutubeOpenGL"
    window.reset(glfwCreateWindow(width, height, title.data(), NULL, NULL));
    // Error check if the window fails to create
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        shouldExit = true;
        return;
    }
    std::cout << "Window " << window.get() << " constructed\n";

    // Introduce the window into the current context
    glfwMakeContextCurrent(window.get());

    // Load GLAD so it configures OpenGL
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return;
    }
    // Specify the viewport of OpenGL in the Window
    // In this case the viewport goes from x = 0, y = 0, to x = 800, y = 800
    glViewport(0, 0, width, height);
    glfwSetFramebufferSizeCallback(window.get(), framebuffer_size_callback);

    // Enables the Depth Buffer
    glEnable(GL_DEPTH_TEST);

    // Enables Multisampling
    // glEnable(GL_MULTISAMPLE);

    // Enables Cull Facing
    // glEnable(GL_CULL_FACE);
    // Keeps front faces
    // glCullFace(GL_FRONT);
    // Uses counter clock-wise standard
    // glFrontFace(GL_CCW);

    // ===================== IMGUI
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
    // io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;   // Enable Docking
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable; // Enable Multi-Viewport / Platform Windows
    // io.ConfigViewportsNoAutoMerge = true;
    // io.ConfigViewportsNoTaskBarIcon = true;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // ImGui::StyleColorsLight();

    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
    ImGuiStyle &style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0F;
        style.Colors[ImGuiCol_WindowBg].w = 1.0F;
    }

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window.get(), true);
    ImGui_ImplOpenGL3_Init(glsl_version.data());

    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'docs/FONTS.md' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    // io.Fonts->AddFontDefault();
    // io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    // io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    // io.Fonts->AddFontFromFileTTF("../..clear_color/misc/fonts/DroidSans.ttf", 16.0f);
    // io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    // ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    // IM_ASSERT(font != NULL);

    // ===================== IMGUI
}

auto Engine::BeginFrame() -> void
{
    float currentFrame = static_cast<float>(glfwGetTime());
    deltaTime = currentFrame - lastFrameTime;
    lastFrameTime = currentFrame;

    int display_w;
    int display_h;
    glfwGetFramebufferSize(window.get(), &display_w, &display_h);
    width = display_w;
    height = display_h;

    shouldExit = glfwWindowShouldClose(window.get());

    glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    calcFps();
}

auto Engine::EndFrame() -> void
{

    // Swap the back buffer with the front buffer
    glfwSwapBuffers(window.get());
    // Take care of all GLFW events
    glfwPollEvents();
}

auto Engine::RenderUIElement(const UIElement &element) -> void
{
    element.DrawUIElement();
}
auto Engine::DrawUIElement() const -> void
{
    bool show = true;
    ImGui::ShowDemoWindow(&show);
}
auto Engine::Finished() const -> bool
{
    return shouldExit;
}

auto Engine::calcFps() -> void
{

    framesSinceUpdate++;

    accumulateDelta += deltaTime;

    if (accumulateDelta >= 1.0F)
    {
        glfwSetWindowTitle(window.get(), (title.data() + std::to_string((framesSinceUpdate / accumulateDelta))).c_str());
        // std::cout << "FPS:" << framesSinceUpdate / accumulateDelta << "\n";

        accumulateDelta = 0.0F;
        framesSinceUpdate = 0;
    }
}

auto Engine::GetWindowPointer() const -> GLFWwindow *
{
    return window.get();
}

auto Engine::BeginUI() -> void
{
    // ===================== IMGUI
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    // ===================== IMGUI
}
auto Engine::EndUI() -> void
{
    // ===================== IMGUI
    ImGuiIO &io = ImGui::GetIO();
    (void)io;
    int display_w;
    int display_h;
    glfwGetFramebufferSize(window.get(), &display_w, &display_h);
    io.DisplaySize = ImVec2(display_w, display_h);

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    // Update and Render additional Platform Windows
    // (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
    //  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)

    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        GLFWwindow *backup_current_context = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        glfwMakeContextCurrent(backup_current_context);
    }
    // ===================== IMGUI
}

auto Engine::framebuffer_size_callback(GLFWwindow *window, int width, int height) -> void
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

auto Engine::GetWindowSize() const -> std::pair<unsigned int, unsigned int>
{
    return {width, height};
}