#pragma once

#include "Bindable.h"
#include <vector>

class EBO : public Bindable
{
public:
	EBO(std::vector<GLuint> &indices);
	EBO(GLuint *indices, GLsizeiptr size);
	virtual ~EBO() override;
	virtual auto GetID() const -> GLuint override;
	virtual auto Bind() const -> void override;
	virtual auto Unbind() const -> void override;
	virtual auto Delete() const -> void override;
};