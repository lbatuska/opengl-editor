#pragma once

#include "Bindable.h"
#include <vector>

#include "Vertex.h"

class VBO : public Bindable
{
public:
	VBO(std::vector<Vertex> &vertices);
	VBO(GLfloat *vertices, GLsizeiptr size);
	virtual ~VBO() override;
	virtual auto GetID() const -> GLuint override;
	virtual auto Bind() const -> void override;
	virtual auto Unbind() const -> void override;
	virtual auto Delete() const -> void override;
};