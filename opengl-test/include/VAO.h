#pragma once

#include "Bindable.h"

#include "VBO.h"

class VAO : public Bindable
{
public:
	VAO();
	virtual ~VAO() override;
	virtual auto LinkAttrib(VBO &VBO, GLuint layout, GLuint numComponents, GLenum type, GLsizeiptr stride, void *offset) -> void;
	virtual auto GetID() const -> GLuint override;
	virtual auto Bind() const -> void override;
	virtual auto Unbind() const -> void override;
	virtual auto Delete() const -> void override;
};