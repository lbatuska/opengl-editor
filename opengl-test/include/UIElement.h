#pragma once

class UIElement
{
protected:
    virtual auto DrawUIElement() const -> void = 0;
    friend class Engine;
};