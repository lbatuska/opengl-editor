#pragma once

#include "Bindable.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cerrno>

class Shader : public Bindable
{
public:
	Shader(const char *vertexFile, const char *fragmentFile);
	virtual ~Shader();
	virtual auto GetID() const -> GLuint override;
	/// <summary>
	/// Use it to Activate the shader program
	/// </summary>
	/// <returns></returns>
	virtual auto Bind() const -> void override;
	inline auto Activate() const -> void { Bind(); };
	virtual auto Unbind() const -> void override;
	virtual auto Delete() const -> void override;

private:
	static auto compileErrors(unsigned int shader, const char *type) -> void;
	auto get_file_contents(const char *filename) const -> std::string;
};