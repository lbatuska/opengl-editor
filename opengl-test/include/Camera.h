#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Shader.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <glm/gtx/string_cast.hpp> // for debugging -> to_string()
#include "UIElement.h"
#include "imgui.h"

#include "Engine.h"

class Camera : public UIElement
{
public:
	// Stores the width and height of the window
	int width;
	int height;

	// Stores the main vectors of the camera
	glm::vec3 Position;
	glm::vec3 Orientation;
	glm::vec3 Up;
	glm::mat4 cameraMatrix;

	// Prevents the camera from jumping around when first clicking left click
	bool firstClick;

	// Adjust the speed of the camera and it's sensitivity when looking around
	float speed;
	float sensitivity;

	// Update camera matrix
	auto UpdateMatrix(float FOVdeg, float nearPlane, float farPlane) -> void;
	auto UpdateMatrix() -> void;
	// Export camera matrix to Vertex Shader
	auto Matrix(Shader &shader, const char *uniform) -> void;
	// Camera constructor to set up initial values
	Camera(int width, int height, glm::vec3 position);
	Camera(const Engine &engine, glm::vec3 position);

	// Updaates and Exports the camera matrix to the Vertex shader
	[[deprecated("Use UpdateMatrix() to update the camera matrix & Matrix() to pass it to the Vertex Shader.")]] void Matrix(float FOVdeg, float nearPlane, float farPlane, Shader &shader, const char *uniform);
	// Handles camera inputs
	void Inputs(GLFWwindow *window);

	// returns the view matrix calculated using Euler Angles and the LookAt Matrix
	auto GetViewMatrix() -> glm::mat4;
	auto GetProjectionMatrix() -> glm::mat4;

protected:
	virtual auto DrawUIElement() const -> void;

private:
	float FOV = 45.0F;
	float nearPlane = 0.1F;
	float farPlane = 100.0F;
};