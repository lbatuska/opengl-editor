#pragma once

// IMGUI
#define IMGUI_IMPL_OPENGL_LOADER_GLAD
// #include "imgui.h"
#include "backends/imgui_impl_opengl3.h"
#include "backends/imgui_impl_glfw.h"
// === OpenGL Related ===
// GLAD
//#include <glad/glad.h>
// GLAD2
#include <glad/glad.h>
#include <GLFW/glfw3.h>
// === Standard Library ===
#include <iostream>
#include <memory>
#include <string_view>
// == = GLM Related == =
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// optional
#include <glm/gtx/string_cast.hpp> // for debugging -> to_string()
// === STB ===
#include "stb_image.h"

#include "UIElement.h"

// Deleter for the unique ptr

class Engine : public UIElement
{
private:
    struct DestroyglfwWin
    {

        void operator()(GLFWwindow *ptr)
        {
            std::cout << "Destroying GLFW Window Context" << std::endl;
            glfwDestroyWindow(ptr);
            // Terminate GLFW
            glfwTerminate();
        }
    };

    unsigned int width;
    unsigned int height;
    float deltaTime;     // Time between current frame and last frame
    float lastFrameTime; // Time of last frame
    size_t framesSinceUpdate;
    float accumulateDelta;
    bool shouldExit;
    std::string_view title;
    std::unique_ptr<GLFWwindow, DestroyglfwWin> window;
    std::string_view glsl_version;
    ImVec4 clear_color;

    static auto glfw_error_callback(int error, const char *description) -> void;
    static auto framebuffer_size_callback(GLFWwindow *window, int width, int height) -> void;

public:
    Engine(unsigned int width = 1920, unsigned int height = 1080,
           const char *title = "OpenGLWindow ", ImVec4 clear_color = ImVec4(0.07f, 0.13f, 0.17f, 1.0f));
    ~Engine();
    auto Initialize() -> void;
    auto BeginFrame() -> void;
    auto EndFrame() -> void;
    auto RenderUIElement(const UIElement &element) -> void;
    auto Finished() const -> bool;
    auto GetWindowPointer() const -> GLFWwindow *;
    auto BeginUI() -> void;
    auto EndUI() -> void;
    auto GetWindowSize() const -> std::pair<unsigned int, unsigned int>;
    virtual auto DrawUIElement() const -> void;

private:
    auto calcFps() -> void;
};