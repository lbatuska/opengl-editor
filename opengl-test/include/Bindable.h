#pragma once

#include <glad/glad.h>

class Bindable
{
protected:
	GLuint ID;

public:
	virtual auto GetID() const -> GLuint = 0;
	virtual auto Bind() const -> void = 0;
	virtual auto Unbind() const -> void = 0;
	virtual auto Delete() const -> void = 0;
	virtual ~Bindable() = default;
};