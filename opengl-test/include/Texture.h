#pragma once

#include "Bindable.h"

#include "stb_image.h"

#include "Shader.h"

class Texture : public Bindable
{
public:
	Texture(const char *image, GLenum texType, GLuint slot, GLenum format, GLenum pixelType);
	virtual ~Texture();
	virtual auto GetID() const -> GLuint override;
	virtual auto Bind() const -> void override;
	virtual auto Unbind() const -> void override;
	virtual auto Delete() const -> void override;
	/// <summary>
	/// Assigns a texture unit to a texture
	/// </summary>
	/// <param name="shader"></param>
	/// <param name="uniform"></param>
	/// <param name="unit"></param>
	static auto texUnit(Shader &shader, const char *uniform, GLuint unit) -> void;
	virtual auto GetType() const -> GLenum;

private:
	GLenum type;
	GLuint unit;
};